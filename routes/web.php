<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* 
 * METODOS HTTP:
 *	GET -> conseguir datos
 *	POST -> almacenar datos
 *	PUT -> actualizar recursos
 *	DELETE -> eleiminar recursos
 *  
 */

/***** RUTAS BASICAS *****/
Route::get('/', function () {
	return view('welcome');
});

//|--------------------------------------------------------------------------

Route::get('/mostrar-fecha', function () {

	$titulo = "Mostrando la fecha en una ruta basica";
	return view('mostrar-fecha', array(
		'titulo' => $titulo
	));
});

//|--------------------------------------------------------------------------


/****** RUTAS CON PARAMETROS *****/
/*Parametro obligatorio */
// los parametros que usemos por la url hemos de recogerlos en la funcion
Route::get('/pelicula1/{titulo}', function ($titulo) {

	return view('pelicula', array(
		'titulo' => $titulo
	));
});

//|--------------------------------------------------------------------------


Route::get('colaboradores/{nombre}', function ($nombre) {
	return "Mostrando el colaborador $nombre";
});

//|--------------------------------------------------------------------------


Route::get('tienda/productos/{id}', function ($id_producto) {
	return "Mostrando el producto $id_producto de la tienda";
});

//|--------------------------------------------------------------------------


/*Parametro opcional */
Route::get('/usuario/{nombre?}', function ($nombre = "Parametro por defecto si no se indica ,Soy pepe") {

	return view('ver-nombre', array(
		'nombre' => $nombre
	));
});

//|--------------------------------------------------------------------------


Route::get('agenda/{mes}/{ano?}', function ($mes, $ano = 2020) {
	return "Viendo la agenda de $mes de $ano";
});

//|--------------------------------------------------------------------------


Route::get('pelicula/{titulo}/{any?}', function ($titulo, $any = 2019) 
{

	return view('pelicula', array(

		'titulo' => $titulo,
		'any'   => $any,
	));
});

//|--------------------------------------------------------------------------


/*condiciones al parametro*/
Route::get('users/{name}/{lastName?}/{year?}', function ($name, $lastName = 'apellidos opcionales', $year = 1984) {

	return view('user', array(

		'name' => $name,
		'lastname' => $lastName,
		'year' => $year

		//validacion con patron tipo de parametros introducidos
	))->where(array(

		'name' => '[A-Za-z]+',
		'lastname' => '[A-Za-z]+',
		'year' => '[0-9]+'
	));
});


//|--------------------------------------------------------------------------

/* USO DE ->WHITH EN VEZ DE PASAR UN ARRAY */
Route::get('/listado-peliculas', function () {

	$titulo = "Listado de peliculas";
	$listado = array("Batman", "Gran Torino" , "Scarface");

	return view('peliculas.listado')
		->with ('titulo', $titulo)
		->with ('listado', $listado);
});



